/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author CLINTON
 */
public class StudentsModel {
    String id,regno,name,gender,contact,dob,home,dor,balance;
    public StudentsModel(String id,String regno,String name,String gender,String contact,String dob,String home,String dor,String balance){
        this.id=id;
        this.regno=regno;
        this.name=name;
        this.gender=gender;
        this.contact=contact;
        this.dob=dob;
        this.home=home;
        this.dor=dor;
        this.balance=balance;
    }
    //id
    public String getId(){
        return id;
    }
    public void setId(String id){
        this.id=id;
    }
    //regno
     public String getRegno(){
        return regno;
    }
    public void setRegno(String regno){
        this.regno=regno;
    }
    //name
     public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
     public String getGender(){
        return gender;
    }
    public void setGender(String gender){
        this.gender=gender;
    }
    //contact
     public String getContact(){
        return contact;
    }
    public void setContact(String contact){
        this.contact=contact;
    }
    //dob
     public String getDob(){
        return dob;
    }
    public void setDob(String dob){
        this.dob=dob;
    }
    //home
     public String getHome(){
        return home;
    }
    public void setHome(String home){
        this.home=home;
    }
    //dor
     public String getDor(){
        return dor;
    }
    public void setDor(String dor){
        this.dor=dor;
    }
    //fee
     public String getBalance(){
        return balance;
    }
    public void setBalance(String balance){
        this.balance=balance;
    }
}
