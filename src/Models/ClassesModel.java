/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author CLINTON
 */
public class ClassesModel {
    String id,name,date;
    int fee;
    public ClassesModel(String id,String name,String date,int fee){
        this.id=id;
        this.name=name;
        this.date=date;
        this.fee=fee;
    }
    public String getId(){
        return id;
    }
    public void setId(String id){
        this.id=id;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getDate(){
        return date;
    }
    public void setDate(String date){
        this.date=date;
    }
    
    public int getFee(){
        return fee;
    }
    public void setFee(int fee){
        this.fee=fee;
    }
}
