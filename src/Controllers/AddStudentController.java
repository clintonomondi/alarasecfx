/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import util.ConnectionUtil;

/**
 * FXML Controller class
 *
 * @author CLINTON
 */
public class AddStudentController implements Initializable {

   @FXML
   private ComboBox genderfd,classfd;
   @FXML
   private Label amountlb,classlb;
   @FXML
   private Text message;
   @FXML
   private Button registerbtn;
   @FXML
    private RadioButton cashradio,mpesaradio,bankradio;
   @FXML
   private DatePicker dobfd,dorfd;
    @FXML
    private TextField balancefd,anualfeefd,namefd,regnofd,contactfd,homefd,trans_idfd,purposefd,amountfd;
     Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;
    int class_id;
    int balance;
    int id;
     ToggleGroup group;
     int student_id;
    
     public AddStudentController(){
         try{
        con = ConnectionUtil.connectdb();
       stmt=con.createStatement();
        }catch(Exception n){
            
        }
    }
  
     public void ClassAction(ActionEvent event){
        try{
            String sql="SELECT * FROM classes WHERE name='"+classfd.getSelectionModel().getSelectedItem()+"'";
            rs=stmt.executeQuery(sql);
            while(rs.next()){
               balance=rs.getInt("fee");
               class_id=rs.getInt("id");
               amountlb.setText(balance+"");
               classlb.setText(classfd.getSelectionModel().getSelectedItem()+"");
               balancefd.setText(balance+"");
               anualfeefd.setText(balance+"");
            }
        }catch(Exception n){
            
        }
     }
public void Register(ActionEvent event){
    try{
        int amount=Integer.parseInt(amountfd.getText());
  if(balance<amount){
      message.setFill(Color.RED); message.setVisible(true);message.setText("Amount cannot be more than the expected fee!");
  }
  else{
     
      //students
      String name=namefd.getText();
      String regno=regnofd.getText();
      String contact=contactfd.getText();
       LocalDate dob = dobfd.getValue();
        LocalDate dor = dorfd.getValue();
        String home=homefd.getText();
        //class_id
        //gender
        int currentbalance=balance-amount;
        if(trans_idfd.getText().isEmpty()|| purposefd.getText().isEmpty()){
            message.setFill(Color.RED); message.setVisible(true);message.setText("Enter Transaction and Purpose!");
        }
        if(name.isEmpty()||regno.isEmpty()||contact.isEmpty()||home.isEmpty()||dor.equals("")||dob.equals("")){
            message.setFill(Color.RED); message.setVisible(true);message.setText("Please enter all fields!");
            
        }
        if(classlb.getText().equals("select class")){
             message.setFill(Color.RED); message.setVisible(true);message.setText("Select class!");
        }
        else{
            String sql="INSERT INTO students VALUES('"+id+"','"+name+"','"+regno+"','"+contact+"','"+dob+"','"+dor
                    +"','"+home+"','"+class_id+"','"+genderfd.getSelectionModel().getSelectedItem()+"','"+currentbalance+"')";
            stmt.executeUpdate(sql);
            String query="SELECT MAX(id) AS student_id FROM students";
            rs=stmt.executeQuery(query);
            while(rs.next()){
                student_id=rs.getInt("student_id");
                Updatefee();
            }
             
        }

  }
    }catch(Exception n){
        System.out.println(n.getMessage());
         message.setFill(Color.RED); message.setVisible(true);message.setText("Amounts should be integer values!");
        
    }
}     
  public void Updatefee(){
      try{
          //student_id
          int amount=Integer.parseInt(amountfd.getText());
           RadioButton selectedradio = (RadioButton) group.getSelectedToggle();
      String mode = selectedradio.getText();
      String trans_id=trans_idfd.getText();
      String purpose=purposefd.getText();
       LocalDate date = dorfd.getValue();
       String sql="INSERT INTO fee VALUES('"+id+"','"+student_id+"','"+amount+"','"+mode+"','"+trans_id+"','"+purpose+"','"+date+"')";
       stmt.executeUpdate(sql);
      message.setFill(Color.GREEN); message.setVisible(true);message.setText("Student admitted successfully!");
      JOptionPane.showMessageDialog(null,"Student admitted successfully,click ok");
       Stage stage=(Stage)registerbtn.getScene().getWindow();
       stage.close();
              
      }catch(Exception n){
          message.setFill(Color.GREEN); message.setVisible(true);message.setText("Cannot update Fee"); 
      }
  }   
     
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        message.setVisible(false);
        genderfd.getItems().removeAll(genderfd.getItems());
    genderfd.getItems().addAll("Select gender", "Male", "Female","Others");
    genderfd.getSelectionModel().select("Select gender");
     classfd.getSelectionModel().select("Select class");
    try{
        String sql="SELECT name FROM classes";
        rs=stmt.executeQuery(sql);
        while(rs.next()){
            classfd.getItems().addAll(rs.getString("name")); 
        }
    }catch(Exception n){
        System.out.println(n.getMessage());
            }
    
     group = new ToggleGroup();
    cashradio.setToggleGroup(group);
     mpesaradio.setToggleGroup(group);
      bankradio.setToggleGroup(group);
    }    
    
}
