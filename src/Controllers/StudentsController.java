/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.StudentsModel;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import util.ConnectionUtil;

/**
 * FXML Controller class
 *
 * @author CLINTON
 */
public class StudentsController implements Initializable {

    @FXML
    private TableView<StudentsModel> table;
    @FXML
    private TableColumn<StudentsModel,String> col_id;
    @FXML
    private TableColumn<StudentsModel,String> col_regno;
    @FXML
    private TableColumn<StudentsModel,String> col_name;
    @FXML
    private TableColumn<StudentsModel,String> col_gender;
    @FXML
    private TableColumn<StudentsModel,String> col_contact;
    @FXML
    private TableColumn<StudentsModel,String> col_dob;
    @FXML
    private TableColumn<StudentsModel,String> col_home;
    @FXML
    private TableColumn<StudentsModel,String> col_dor;
    @FXML
    private TableColumn<StudentsModel,String> col_balance;
    @FXML
    private Label totallb;
    Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;
     ObservableList<StudentsModel> oblist=FXCollections.observableArrayList();
    
    public StudentsController(){
         try{
        con = ConnectionUtil.connectdb();
       stmt=con.createStatement();
       loaddata();
        }catch(Exception n){
            
        }
    }
    public void loaddata(){
         try{
        String sql="SELECT * FROM students";
        rs=stmt.executeQuery(sql);
        while(rs.next()){
            oblist.add(new StudentsModel(rs.getString("id"),rs.getString("regno"),rs.getString("name"),
                    rs.getString("gender"),rs.getString("contact"),rs.getString("dob"),rs.getString("home"),
                    rs.getString("dor"),rs.getString("balance")));
        }
        }catch(Exception n){
            System.out.println(n.getMessage());
        }
    }
    
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
         col_regno.setCellValueFactory(new PropertyValueFactory<>("regno"));
        col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        col_gender.setCellValueFactory(new PropertyValueFactory<>("gender"));
        col_contact.setCellValueFactory(new PropertyValueFactory<>("contact"));
        col_dob.setCellValueFactory(new PropertyValueFactory<>("dob"));
        col_home.setCellValueFactory(new PropertyValueFactory<>("home"));
        col_dor.setCellValueFactory(new PropertyValueFactory<>("dor"));
        col_balance.setCellValueFactory(new PropertyValueFactory<>("balance"));
        table.setItems(oblist);
        totallb.setText(table.getItems().size()+"");
    }    
    
}
