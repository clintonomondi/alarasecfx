/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.ClassesModel;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import util.ConnectionUtil;

/**
 * FXML Controller class
 *
 * @author CLINTON
 */
public class ClassesController implements Initializable {

    @FXML
    private TextField namefd;
    @FXML
    private TextField feefd;
    @FXML
    private DatePicker datefd;
    @FXML
    private Text message;
    @FXML
    private TableView<ClassesModel> table;
    @FXML
    private TableColumn<ClassesModel,String> col_id;
    @FXML
    private TableColumn<ClassesModel,String> col_name;
    @FXML
    private TableColumn<ClassesModel,String> col_date;
    @FXML
    private TableColumn<ClassesModel,String> col_fee;
    @FXML
    private Button submitbtn;
    @FXML
    private Button updatebtn;
    @FXML
    private Button deletebtn;
     Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;
    int id;
    String id2;
    ObservableList<ClassesModel> oblist=FXCollections.observableArrayList();
    
    public ClassesController(){
         try{
        con = ConnectionUtil.connectdb();
       stmt=con.createStatement();
       loaddata();
        }catch(Exception n){
            
        }
    }
    
    public void Submit(ActionEvent event){
        try{
            String name=namefd.getText();
            LocalDate date = datefd.getValue();
            int fee=Integer.parseInt(feefd.getText());
            if(name.isEmpty() || date.equals("")){
                 message.setFill(Color.RED); message.setVisible(true);message.setText("Please enter all fields correctly!");
            }
            else{
            String sql="INSERT INTO classes VALUES('"+id+"','"+name+"','"+fee+"','"+date+"')";
            stmt.executeUpdate(sql);
           message.setFill(Color.GREEN); message.setVisible(true);message.setText("Class submitted successfully");
            checkdata();
           loaddata();
           namefd.setText("");
            }
        }catch(Exception n){
             message.setFill(Color.RED); message.setVisible(true);message.setText("Enter fee as Integer value");
            System.out.println(n.getMessage());
        }
    }
    public void checkdata(){
      for ( int i = 0; i<table.getItems().size(); i++) {
    table.getItems().clear();
}
    }
    
    public void loaddata(){
       try{
        String sql="SELECT * FROM classes";
        rs=stmt.executeQuery(sql);
        while(rs.next()){
            oblist.add(new ClassesModel(rs.getString("id"),rs.getString("name"),rs.getString("date"),rs.getInt("fee")));
        }
        }catch(Exception n){
            System.out.println(n.getMessage());
        }
        
    }
    
    public void Display(MouseEvent event){
         if (event.getClickCount() > 1) {
      ClassesModel row = table.getSelectionModel().getSelectedItem();
        namefd.setText(row.getName());
        feefd.setText(row.getFee()+"");
        id2=row.getId();
    }
    }
    
    
    public void Update(ActionEvent event){
        try{
            String name=namefd.getText();
            int fee=Integer.parseInt(feefd.getText());
            if(name.isEmpty()){
                 message.setFill(Color.RED); message.setVisible(true);message.setText("Please enter name field to update!");
            }
            else{
                String sql="UPDATE classes SET name='"+name+"',fee='"+fee+"' WHERE id='"+id2+"'";
                stmt.executeUpdate(sql);
                 message.setFill(Color.GREEN); message.setVisible(true);message.setText("Class  updated successfully");
            checkdata();
           loaddata();
           namefd.setText("");
            }
            
        }catch(Exception n){
             message.setFill(Color.RED); message.setVisible(true);message.setText("Enter fee as integer valu");
            System.out.println(n.getMessage());
        }
    }
    public void Delete(ActionEvent event){
       try{
                String sql="DELETE FROM classes where id='"+id2+"'";
                stmt.executeUpdate(sql);
                 message.setFill(Color.GREEN); message.setVisible(true);message.setText("Class deleted successfully");
            checkdata();
           loaddata();
           namefd.setText("");
        }catch(Exception n){
            System.out.println(n.getMessage());
        } 
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        message.setVisible(false);
        col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
        col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
        col_date.setCellValueFactory(new PropertyValueFactory<>("date"));
        col_fee.setCellValueFactory(new PropertyValueFactory<>("fee"));
        table.setItems(oblist);
         final Tooltip tooltip = new Tooltip();
       tooltip.setText("Double click to edit");
       table.setTooltip(tooltip);
    }    
    
}
