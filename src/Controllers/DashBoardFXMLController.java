/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author CLINTON
 */
public class DashBoardFXMLController implements Initializable {

    @FXML
    private Text title;
     @FXML
  private   Pane buttonpane;
      @FXML
      private Button classesbtn,studentsbtn;
      @FXML
      private Button addstudent;
      
    public void Classes(ActionEvent event){
        try{
            Stage stage=new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/alarasecfx/Classes.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.show();
        }catch(Exception n){
            
        }
    }
    
    public void Addstudent(ActionEvent event){
        try{
            Stage stage=new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/alarasecfx/AddStudent.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.show();
        }catch(Exception n){
            
        }
    }
    public void Students(ActionEvent event){
          try{
            Stage stage=new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/alarasecfx/Students.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.show();
        }catch(Exception n){
            
        }
    }
    
    public void initialize(URL url, ResourceBundle rb) {
       Reflection r = new Reflection();
        r.setFraction(0.7f);
        buttonpane.setEffect(r);
    //DropShadow effect
        DropShadow dropShadow = new DropShadow();
        dropShadow.setOffsetX(5);
        dropShadow.setOffsetY(5);
       
        //Adding text and DropShadow effect to it
        title.setFont(Font.font("Courier New", FontWeight.BOLD, 28));
        title.setEffect(dropShadow);
    }    
    
}
