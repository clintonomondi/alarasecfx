/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import util.ConnectionUtil;

/**
 *
 * @author CLINTON
 */
public class LoginController implements Initializable {
    
     @FXML
    private Text message;
    @FXML
    private Text title;
    @FXML
  private   Pane formpane;
     @FXML
     private TextField emailfd;
      @FXML
     private PasswordField passwordfd;
      @FXML
     public Button btnlogin;
      @FXML
      Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;
    
    public LoginController(){
        try{
        con = ConnectionUtil.connectdb();
       stmt=con.createStatement();
        }catch(Exception n){
            
        }
    }
    public void ValidateLogin(ActionEvent event) {
        try{
            String sql="SELECT * FROM admin WHERE email='"+emailfd.getText()+"' && password='"+passwordfd.getText()+"'";
            rs=stmt.executeQuery(sql);
            if(rs.next()){
                 Node  node=(Node)event.getSource();
       Stage stage=(Stage)node.getScene().getWindow();
       stage.close();
       Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/alarasecfx/DashBoardFXML.fxml")));
       stage.setScene(scene);
       stage.setResizable(false);
        stage.show();
            }
            else{
                 message.setFill(Color.RED); message.setVisible(true);message.setText("Invalid Email or Password");
            }
      
        }catch(Exception n){
            System.out.println(n);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        message.setVisible(false);
        Reflection r = new Reflection();
        r.setFraction(0.7f);
        formpane.setEffect(r);
    //DropShadow effect
        DropShadow dropShadow = new DropShadow();
        dropShadow.setOffsetX(5);
        dropShadow.setOffsetY(5);
       
        //Adding text and DropShadow effect to it
        title.setFont(Font.font("Courier New", FontWeight.BOLD, 28));
        title.setEffect(dropShadow);
    }    
    
}
